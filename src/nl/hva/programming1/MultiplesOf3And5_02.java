package nl.hva.programming1;

public class MultiplesOf3And5_02 {

    public static void main(String[] args) {
        // write your code here
        getSumOfMultiples();
        ;

    }

    public static void getSumOfMultiples() {
        int sumOfMultiples3 = 0;
        int sumOfMultiples5 = 0;
        int sumOfMultiples15 = 0;
        for (int i = 0; i < 1000; i = i + 3) {

            sumOfMultiples3 += i;
        }

        for (int i = 0; i < 1000; i = i + 5) {

            sumOfMultiples5 += i;
        }

        for (int i = 0; i < 1000; i = i + 15) {

            sumOfMultiples15 += i;
        }
        System.out.printf("The sum of multiples = %s", (sumOfMultiples3 + sumOfMultiples5 - sumOfMultiples15));
    }
}

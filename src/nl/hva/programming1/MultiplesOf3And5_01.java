package nl.hva.programming1;

public class MultiplesOf3And5_01 {

    public static void main(String[] args) {
        // write your code here
        getSumOfMultiples();
        ;

    }

    public static void getSumOfMultiples() {
        int sumOfMultiples = 0;
        for (int i = 0; i < 1000; i++) {
            if (i % 3 == 0 || i % 5 == 0)
                sumOfMultiples += i;
        }
        System.out.printf("The sum of multiples = %s", sumOfMultiples);
    }
}
